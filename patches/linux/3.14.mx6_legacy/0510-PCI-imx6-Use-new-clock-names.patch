From 57526136532408bacf2f68c26027abc2924b45d1 Mon Sep 17 00:00:00 2001
From: Lucas Stach <l.stach@pengutronix.de>
Date: Fri, 28 Mar 2014 17:52:55 +0100
Subject: [PATCH] PCI: imx6: Use new clock names

As defined in the new binding.

Signed-off-by: Lucas Stach <l.stach@pengutronix.de>
Signed-off-by: Bjorn Helgaas <bhelgaas@google.com>
Acked-by: Richard Zhu <r65037@freescale.com>
---
 drivers/pci/host/pci-imx6.c | 74 ++++++++++++++++++---------------------------
 1 file changed, 29 insertions(+), 45 deletions(-)

Index: linux-imx_3.14.52_1.1.0_ga/drivers/pci/host/pci-imx6.c
===================================================================
--- linux-imx_3.14.52_1.1.0_ga.orig/drivers/pci/host/pci-imx6.c
+++ linux-imx_3.14.52_1.1.0_ga/drivers/pci/host/pci-imx6.c
@@ -51,8 +51,6 @@ struct imx6_pcie {
	int			power_on_gpio;
	struct clk		*pcie_bus;
	struct clk		*pcie_phy;
-	struct clk		*pcie_inbound_axi;
-	struct clk		*ref_100m;
	struct clk		*pcie;
	struct pcie_port	pp;
	struct regmap		*iomuxc_gpr;
@@ -261,6 +259,7 @@ static int imx6_pcie_assert_core_reset(s
 {
	struct imx6_pcie *imx6_pcie = to_imx6_pcie(pp);
	u32 val, gpr1, gpr12;
+	int ret = 0;

	if (is_imx7d_pcie(imx6_pcie)) {
		/* G_RST */
@@ -298,6 +297,30 @@ static int imx6_pcie_assert_core_reset(s

		if ((gpr1 & IMX6Q_GPR1_PCIE_REF_CLK_EN) &&
		    (gpr12 & IMX6Q_GPR12_PCIE_CTL_2)) {
+			request_bus_freq(BUS_FREQ_HIGH);
+			ret = clk_prepare_enable(imx6_pcie->pcie_phy);
+			if (ret) {
+				dev_err(pp->dev, "unable to enable pcie_phy clock\n");
+				goto err_pcie_phy;
+			}
+
+			if (!IS_ENABLED(CONFIG_EP_MODE_IN_EP_RC_SYS)
+					&& !IS_ENABLED(CONFIG_RC_MODE_IN_EP_RC_SYS)) {
+				ret = clk_prepare_enable(imx6_pcie->pcie_bus);
+				if (ret) {
+					dev_err(pp->dev, "unable to enable pcie_bus clock\n");
+					goto err_pcie_bus;
+				}
+			}
+
+			ret = clk_prepare_enable(imx6_pcie->pcie);
+			if (ret) {
+				dev_err(pp->dev, "unable to enable pcie clock\n");
+				goto err_pcie;
+			}
+
+			usleep_range(200, 500);
+
			val = readl(pp->dbi_base + PCIE_PL_PFLR);
			val &= ~PCIE_PL_PFLR_LINK_STATE_MASK;
			val |= PCIE_PL_PFLR_FORCE_LINK;
@@ -314,7 +337,12 @@ static int imx6_pcie_assert_core_reset(s
				IMX6Q_GPR1_PCIE_REF_CLK_EN, 0);
	}

-	return 0;
+err_pcie:
+	clk_disable_unprepare(imx6_pcie->pcie_bus);
+err_pcie_bus:
+	clk_disable_unprepare(imx6_pcie->pcie_phy);
+err_pcie_phy:
+	return ret;
 }

 static void pci_imx_phy_pll_locked(struct imx6_pcie *imx6_pcie)
@@ -363,37 +391,22 @@ static int imx6_pcie_deassert_core_reset
		goto err_pcie;
	}

-	if (is_imx6sx_pcie(imx6_pcie)) {
-		ret = clk_prepare_enable(imx6_pcie->pcie_inbound_axi);
-		if (ret) {
-			dev_err(pp->dev, "unable to enable pcie clock\n");
-			goto err_inbound_axi;
-		}
+	/* power up core phy and enable ref clock */
+	regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR1,
+			IMX6Q_GPR1_PCIE_TEST_PD, 0);

-		regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR12,
-				IMX6SX_GPR12_PCIE_TEST_PD, 0);
-	} else if (!is_imx7d_pcie(imx6_pcie)) {
-		/* power up core phy and enable ref clock */
-		regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR1,
-				IMX6Q_GPR1_PCIE_TEST_PD, 0);
+	/*
+	 * the async reset input need ref clock to sync internally,
+	 * when the ref clock comes after reset, internal synced
+	 * reset time is too short , cannot meet the requirement.
+	 * add one ~10us delay here.
+	 */
+	udelay(10);
+	regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR1,
+			IMX6Q_GPR1_PCIE_REF_CLK_EN,
+			IMX6Q_GPR1_PCIE_REF_CLK_EN);

-		/* sata_ref is not used by pcie on imx6sx */
-		ret = clk_prepare_enable(imx6_pcie->ref_100m);
-		if (ret) {
-			dev_err(pp->dev, "unable to enable ref_100m\n");
-			goto err_inbound_axi;
-		}
-		/*
-		 * the async reset input need ref clock to sync internally,
-		 * when the ref clock comes after reset, internal synced
-		 * reset time is too short , cannot meet the requirement.
-		 * add one ~10us delay here.
-		 */
-		udelay(10);
-		regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR1,
-				IMX6Q_GPR1_PCIE_REF_CLK_EN,
-				IMX6Q_GPR1_PCIE_REF_CLK_EN);
-	}
+	usleep_range(200, 500);

	/* Some boards don't have PCIe reset GPIO. */
	if (gpio_is_valid(imx6_pcie->reset_gpio)) {
@@ -452,8 +465,6 @@ static int imx6_pcie_deassert_core_reset

	return 0;

-err_inbound_axi:
-	clk_disable_unprepare(imx6_pcie->pcie);
 err_pcie:
	if (!IS_ENABLED(CONFIG_EP_MODE_IN_EP_RC_SYS)
			&& !IS_ENABLED(CONFIG_RC_MODE_IN_EP_RC_SYS))
@@ -625,25 +636,7 @@ out:
			&& !IS_ENABLED(CONFIG_RC_MODE_IN_EP_RC_SYS))
			clk_disable_unprepare(imx6_pcie->pcie_bus);
		clk_disable_unprepare(imx6_pcie->pcie_phy);
-		if (is_imx6sx_pcie(imx6_pcie)) {
-			/* Disable clks and power down PCIe PHY */
-			clk_disable_unprepare(imx6_pcie->pcie_inbound_axi);
-			release_bus_freq(BUS_FREQ_HIGH);
-
-			/*
-			 * Power down PCIe PHY.
-			 */
-			regulator_disable(imx6_pcie->pcie_phy_regulator);
-		} else if (is_imx7d_pcie(imx6_pcie)) {
-			release_bus_freq(BUS_FREQ_HIGH);
-			/*
-			 * Power down PCIe PHY.
-			 */
-			regulator_disable(imx6_pcie->pcie_phy_regulator);
-		} else {
-			clk_disable_unprepare(imx6_pcie->ref_100m);
-			release_bus_freq(BUS_FREQ_HIGH);
-		}
+		release_bus_freq(BUS_FREQ_HIGH);
	} else {
		tmp = readl(pp->dbi_base + 0x80);
		dev_dbg(pp->dev, "Link up, Gen=%i\n", (tmp >> 16) & 0xf);
@@ -1029,148 +1022,31 @@ static int pci_imx_suspend_noirq(struct

	pci_imx_pm_turn_off(imx6_pcie);

-	if (is_imx6sx_pcie(imx6_pcie) || is_imx7d_pcie(imx6_pcie)) {
-		/* Disable clks */
-		clk_disable_unprepare(imx6_pcie->pcie);
-		clk_disable_unprepare(imx6_pcie->pcie_phy);
-		clk_disable_unprepare(imx6_pcie->pcie_bus);
-		if (is_imx6sx_pcie(imx6_pcie)) {
-			clk_disable_unprepare(imx6_pcie->pcie_inbound_axi);
-		} else {
-			/* turn off external osc input */
-			regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR12,
-					BIT(5), BIT(5));
-		}
-		release_bus_freq(BUS_FREQ_HIGH);
-
-		/* Power down PCIe PHY. */
-		regulator_disable(imx6_pcie->pcie_phy_regulator);
-		if (gpio_is_valid(imx6_pcie->power_on_gpio))
-			gpio_set_value(imx6_pcie->power_on_gpio, 0);
-	} else if (is_imx6qp_pcie(imx6_pcie)) {
-		regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR1,
-				IMX6Q_GPR1_PCIE_REF_CLK_EN, 0);
-		clk_disable_unprepare(imx6_pcie->pcie_bus);
-		clk_disable_unprepare(imx6_pcie->ref_100m);
-		clk_disable_unprepare(imx6_pcie->pcie_phy);
-		clk_disable_unprepare(imx6_pcie->pcie);
-		release_bus_freq(BUS_FREQ_HIGH);
-	} else {
-		/*
-		 * L2 can exit by 'reset' or Inband beacon (from remote EP)
-		 * toggling phy_powerdown has same effect as 'inband beacon'
-		 * So, toggle bit18 of GPR1, used as a workaround of errata
-		 * "PCIe PCIe does not support L2 Power Down"
-		 */
-		regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR1,
-				IMX6Q_GPR1_PCIE_TEST_PD,
-				IMX6Q_GPR1_PCIE_TEST_PD);
-	}
+	/*
+	 * L2 can exit by 'reset' or Inband beacon (from remote EP)
+	 * toggling phy_powerdown has same effect as 'inband beacon'
+	 * So, toggle bit18 of GPR1, used as a workaround of errata
+	 * "PCIe PCIe does not support L2 Power Down"
+	 */
+	regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR1,
+			IMX6Q_GPR1_PCIE_TEST_PD,
+			IMX6Q_GPR1_PCIE_TEST_PD);

	return 0;
 }

 static int pci_imx_resume_noirq(struct device *dev)
 {
-	int ret = 0;
	struct imx6_pcie *imx6_pcie = dev_get_drvdata(dev);
-	struct pcie_port *pp = &imx6_pcie->pp;
-
-	if (is_imx6sx_pcie(imx6_pcie) || is_imx7d_pcie(imx6_pcie)) {
-		if (is_imx7d_pcie(imx6_pcie))
-			regmap_update_bits(imx6_pcie->reg_src, 0x2c, BIT(6), 0);
-		else
-			regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR12,
-					IMX6Q_GPR12_PCIE_CTL_2, 0);

-		imx6_pcie_assert_core_reset(pp);
-
-		ret = imx6_pcie_init_phy(pp);
-		if (ret < 0)
-			return ret;
-
-		ret = imx6_pcie_deassert_core_reset(pp);
-		if (ret < 0)
-			return ret;
-
-		/*
-		 * controller maybe turn off, re-configure again
-		 */
-		dw_pcie_setup_rc(pp);
-
-		if (IS_ENABLED(CONFIG_PCI_MSI))
-			dw_pcie_msi_cfg_restore(pp);
-
-		if (is_imx7d_pcie(imx6_pcie)) {
-			/* wait for phy pll lock firstly. */
-			pci_imx_phy_pll_locked(imx6_pcie);
-			regmap_update_bits(imx6_pcie->reg_src, 0x2c,
-					BIT(6), BIT(6));
-		} else {
-			regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR12,
-					IMX6Q_GPR12_PCIE_CTL_2,
-					IMX6Q_GPR12_PCIE_CTL_2);
-		}
-		ret = imx6_pcie_wait_for_link(pp);
-		if (ret < 0)
-			pr_info("pcie link is down after resume.\n");
-	} else if (is_imx6qp_pcie(imx6_pcie)) {
-		regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR12,
-				IMX6Q_GPR12_PCIE_CTL_2, 0);
-
-		/* Reset iMX6QP PCIe */
-		regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR1,
-				IMX6Q_GPR1_PCIE_SW_RST, IMX6Q_GPR1_PCIE_SW_RST);
-
-		request_bus_freq(BUS_FREQ_HIGH);
-		clk_prepare_enable(imx6_pcie->pcie);
-		clk_prepare_enable(imx6_pcie->ref_100m);
-		clk_prepare_enable(imx6_pcie->pcie_phy);
-		clk_prepare_enable(imx6_pcie->pcie_bus);
-
-		udelay(10);
-		regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR1,
-				IMX6Q_GPR1_PCIE_REF_CLK_EN,
-				IMX6Q_GPR1_PCIE_REF_CLK_EN);
-
-		if (gpio_is_valid(imx6_pcie->reset_gpio)) {
-			gpio_set_value_cansleep(imx6_pcie->reset_gpio, 1);
-			mdelay(20);
-		} else {
-			/* allow the clocks to stabilize */
-			udelay(200);
-		}
-
-		regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR1,
-				IMX6Q_GPR1_PCIE_SW_RST, 0);
-		/*
-		 * controller maybe turn off, re-configure again
-		 */
-		dw_pcie_setup_rc(pp);
-
-		if (IS_ENABLED(CONFIG_PCI_MSI))
-			dw_pcie_msi_cfg_restore(pp);
-
-		/* Start LTSSM. */
-		regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR12,
-				IMX6Q_GPR12_PCIE_CTL_2,
-				IMX6Q_GPR12_PCIE_CTL_2);
-
-		ret = imx6_pcie_wait_for_link(pp);
-		if (ret < 0)
-			pr_info("pcie link is down after resume back.\n");
-		/* Delay for a while, give ep some times to resume from D3 */
-		udelay(2000);
-	} else {
-		/*
-		 * L2 can exit by 'reset' or Inband beacon (from remote EP)
-		 * toggling phy_powerdown has same effect as 'inband beacon'
-		 * So, toggle bit18 of GPR1, used as a workaround of errata
-		 * "PCIe PCIe does not support L2 Power Down"
-		 */
-		regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR1,
-				IMX6Q_GPR1_PCIE_TEST_PD, 0);
-	}
+	/*
+	 * L2 can exit by 'reset' or Inband beacon (from remote EP)
+	 * toggling phy_powerdown has same effect as 'inband beacon'
+	 * So, toggle bit18 of GPR1, used as a workaround of errata
+	 * "PCIe PCIe does not support L2 Power Down"
+	 */
+	regmap_update_bits(imx6_pcie->iomuxc_gpr, IOMUXC_GPR1,
+			IMX6Q_GPR1_PCIE_TEST_PD, 0);

	return 0;
 }
@@ -1271,47 +1147,8 @@ static int __init imx6_pcie_probe(struct
		return PTR_ERR(imx6_pcie->pcie);
	}

-	if (is_imx7d_pcie(imx6_pcie)) {
-		imx6_pcie->iomuxc_gpr =
-			 syscon_regmap_lookup_by_compatible
-			 ("fsl,imx7d-iomuxc-gpr");
-		imx6_pcie->reg_src =
-			 syscon_regmap_lookup_by_compatible("fsl,imx7d-src");
-		if (IS_ERR(imx6_pcie->reg_src)) {
-			dev_err(&pdev->dev,
-				"imx7d pcie phy src missing or invalid\n");
-			return PTR_ERR(imx6_pcie->reg_src);
-		}
-		imx6_pcie->pcie_phy_regulator = devm_regulator_get(pp->dev,
-				"pcie-phy");
-	} else if (is_imx6sx_pcie(imx6_pcie)) {
-		imx6_pcie->pcie_inbound_axi = devm_clk_get(&pdev->dev,
-				"pcie_inbound_axi");
-		if (IS_ERR(imx6_pcie->pcie_inbound_axi)) {
-			dev_err(&pdev->dev,
-				"pcie clock source missing or invalid\n");
-			return PTR_ERR(imx6_pcie->pcie_inbound_axi);
-		}
-
-		imx6_pcie->pcie_phy_regulator = devm_regulator_get(pp->dev,
-				"pcie-phy");
-
-		imx6_pcie->iomuxc_gpr =
-			 syscon_regmap_lookup_by_compatible
-			 ("fsl,imx6sx-iomuxc-gpr");
-	} else {
-		/* sata_ref is not used by pcie on imx6sx */
-		imx6_pcie->ref_100m = devm_clk_get(&pdev->dev, "ref_100m");
-		if (IS_ERR(imx6_pcie->ref_100m)) {
-			dev_err(&pdev->dev,
-				"ref_100m clock source missing or invalid\n");
-			return PTR_ERR(imx6_pcie->ref_100m);
-		}
-
-		imx6_pcie->iomuxc_gpr =
-			syscon_regmap_lookup_by_compatible
-			("fsl,imx6q-iomuxc-gpr");
-	}
+	imx6_pcie->iomuxc_gpr =
+		syscon_regmap_lookup_by_compatible("fsl,imx6q-iomuxc-gpr");
	if (IS_ERR(imx6_pcie->iomuxc_gpr)) {
		dev_err(&pdev->dev, "unable to find iomuxc registers\n");
		return PTR_ERR(imx6_pcie->iomuxc_gpr);
