# HSMC_D0 -> PIN_M10
set_location_assignment PIN_M10 -to led

# PCIE_CLK
set_instance_assignment -name IO_STANDARD "1.5-V PCML" -to clk
set_location_assignment PIN_V4 -to clk
set_location_assignment PIN_U4 -to "clk(n)"
