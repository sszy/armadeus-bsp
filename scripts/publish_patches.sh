#!/bin/bash

help()
{
	echo
	echo "This script will publish Armadeus Linux and U-Boot current/stable patches."
	echo "This way we ease Armadeus board integration in build systems, like Buildroot for example."
	echo
}

generate_patch()
{
	ALL_VERSIONS=`ls $THE_PATCH_DIR/..`
	echo
	PS3="Choose the $THE_NAME you want to publish: "
	select THE_VERSION in $ALL_VERSIONS
	do
		if [ "$THE_VERSION" == "" ]; then
			exit 1
		else
			echo "You choosed $THE_VERSION"
			break
		fi
	done

	echo "--- Merging patches:"
	FINAL_PATCH=$THE_NAME-$THE_VERSION-armadeus.patch
	rm -f $FINAL_PATCH
	touch $FINAL_PATCH
	THE_PATCHES=`ls $THE_PATCH_DIR/../$THE_VERSION`
	for patch in $THE_PATCHES; do
		echo "Merging $patch"
		cat $THE_PATCH_DIR/../$THE_VERSION/$patch >> $FINAL_PATCH
	#	sleep 1
	done

	echo
	echo "--- Compressing:"
	tar cvzf $FINAL_PATCH.tar.gz $FINAL_PATCH
	if [ "$?" == 0 ]; then
		rm $FINAL_PATCH
	fi
}

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
	help
	exit 1
fi

echo "--- Getting envt variables:"
THIS_DIR=`dirname $0`
make -C $THIS_DIR/.. shell_env
source $THIS_DIR/../armadeus_env.sh

# Linux
THE_PATCH_DIR="$ARMADEUS_LINUX_PATCH_DIR"
THE_NAME="linux"
generate_patch

# U-Boot
THE_PATCH_DIR="$ARMADEUS_U_BOOT_PATCH_DIR"
THE_NAME="u-boot"
generate_patch

echo
echo "--- Uploading archive:"
HOST=ftp2.armadeus.com
echo "username for $HOST ?"
read -p "> " USER
echo "password ?"
read -p "> " PASSWD
if [ "$USER" == "" ] || [ "$PASSWD" == "" ]; then
	echo "aborting"
	exit 1
fi
ftp -n $HOST <<END_SCRIPT
quote USER $USER
quote PASS $PASSWD
cd download
put $FINAL_PATCH.tar.gz
quit
END_SCRIPT

if [ "$?" == 0 ]; then
	echo "done !"
fi

exit 0
