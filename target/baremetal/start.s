/*
	Inspired from imx6q.s (http://atose.org/?p=141), which is:
	Copyright (c) 2013 Andrew Trotman
	Open Source under the BSD License.
*/

.global _Reset

/*
	RESET_HANDLER
	-------------
*/
_Reset:
	ldr sp, =stack_top
	b main
