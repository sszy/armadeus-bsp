/*
        Inspired from imx6q_uart.c (http://atose.org/?p=141), which is:
	Copyright (c) 2012-2013 Andrew Trotman
	Licensed BSD
*/

#include <stdint.h>

#ifdef DEBUG
#include "imx6_uart.h"
#endif

#define CCM_CCGR1	(*(volatile uint32_t *)0x020c406c)
#define CCGR1_CG6	(0x03 << 12)
#define PRE_PERIPH_CLK_SEL	(0x03 << 18)
#define CCM_CBCDR	(*(volatile uint32_t *)0x020c4014)
#define AHB_PODF	(0x7 << 10)
#define IPG_PODF	(0x3 << 8)
#define CCM_CBCMR	(*(volatile uint32_t *)0x020c4018)

#define EPIT1_BASE	(0x020d0000)
#define EPIT1(REG)	(*(volatile uint32_t *)(EPIT1_BASE + REG))
#define EPIT1_WR(REG, VAL)	(*(volatile uint32_t *)(EPIT1_BASE + REG) = VAL)
#define CR		(0x00)
#define CR_SWR		(1 << 16)
#define CR_CLKSRC(x)	(((x) & 0x3) << 24)
#define CR_PRESCALAR(x)	(((x) & 0xfff) << 4)
#define CR_IOVW		(1 << 17)
#define CR_RLD		(1 << 3)
#define CR_ENMOD	(1 << 1)
#define CR_EN		(1 << 0)
#define SR		(0x04)
#define SR_OCIF		(1 << 0)
#define LR		(0x08)

void epit_init(void)
{
	uint32_t speed_in_Hz[] = {528000000, 396000000, 307000000 /*352000000*/, 198000000, 594000000};
	uint32_t frequency;

#ifdef DEBUG
	debug_puts("epit_init \r\n");
#endif
	//CCM_CCGR1 = (CCM_CCGR1 | CCGR1_CG6);       // turn on the clock
 
	/* Software reset the subsystem */
	EPIT1_WR(CR, CR_SWR);
	while ((EPIT1(CR) & CR_SWR) != 0)
		;  /* Wait end of reset */
 
	/*
	   Configure the timer:
	      Use the peripheral clock
	      Set and forget mode
	      Immediate load starting with the value in the load register
	*/
	/*frequency = 528000000 */
	frequency = speed_in_Hz[((CCM_CBCMR & PRE_PERIPH_CLK_SEL) >> 18) & 0x3] /
			(((CCM_CBCDR & AHB_PODF) >> 10) + 1) / (((CCM_CBCDR & IPG_PODF) >> 8) + 1);
	EPIT1_WR(CR, CR_CLKSRC(1) | CR_PRESCALAR((frequency / 1000000) - 1) | CR_RLD | CR_IOVW | CR_ENMOD);
}
 
static void epit_start(uint32_t time_in_us)
{
	/* Store the count value in the load register (which then gets immediately loaded into the timer */
	EPIT1_WR(LR, time_in_us);
 
	/* Clear the status register so that we can watch it clock over */
	EPIT1_WR(SR, /*EPIT1(SR) &*/ SR_OCIF);
	 
	/* Start the timer */
	EPIT1_WR(CR, 0x0102041b /*EPIT1(CR) & CR_EN*/); // SET macro ?
}
 
static void epit_stop(void)
{
	EPIT1_WR(CR, 0x0102041a /*EPIT1(CR) | ~CR_EN*/); // CLR macro ?
}
 
void epit_delay_us(uint32_t usecs)
{
	epit_start(usecs);
	while (EPIT1(SR) == 0)
		;
	epit_stop();
}
